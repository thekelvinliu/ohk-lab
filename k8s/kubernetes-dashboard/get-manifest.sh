#!/usr/bin/env bash

set -e

# directory containing this script
DIR=$(dirname "$0")
FOUT=$DIR/manifest.yml

# use https://github.com/kubernetes/dashboard/blob/v2.0.4/aio/deploy/alternative.yaml
REF="v2.0.4"
SRC="https://raw.githubusercontent.com/kubernetes/dashboard/$REF/aio/deploy/alternative.yaml"
curl -s "$SRC" > "$FOUT"
echo -e "saved base to $CYAN$FOUT$RESET"
