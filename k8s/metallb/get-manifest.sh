#!/usr/bin/env bash

set -e

# directory containing this script
DIR=$(dirname "$0")
FOUT=$DIR/manifest.yml

# use https://github.com/metallb/metallb/blob/v0.9.3/manifests/kustomization.yaml
REF="v0.9.3"
SRC="github.com:metallb/metallb/manifests?ref=$REF"
kubectl kustomize "$SRC" > "$FOUT"
echo -e "saved manifest to $CYAN$FOUT$RESET"
