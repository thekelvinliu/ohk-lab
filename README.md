# ohk-lab

a k3s lab on odroid nodes

## playbooks

### main

```sh
ansible-playbook site.yml -bK
```

### others

```sh
# full system upgrade
ansible-playbook upgrade.yml -K

# quick and dirty teardown
ansible-playbook uninstall.yml -K
```
